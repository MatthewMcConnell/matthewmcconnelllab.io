---
title: "About"
last_update: "2020-06-24"
---

## General

Hello! I am a Computing Science Student at the University of Glasgow completing a MSci. I am originally from Scotland and generally have an interest in anything that could potentially improve and make the world a better place.

![Me!](../images/me.jpg)

## Developer

Much of my enjoyment of being a software developer comes from solving problems. I get excited to solve new problems every day and hopefully rise to meet the challenge.

Some of my experience and skills include:
- *Programming Languages*
  - Python, Java, Haskell, PHP, C#, C, HTML & CSS, Javascript
- *Frameworks*
  - Django, Gatsby, Angular, React, Vue

## University

At the time of writing I am going into my final year at the University of Glasgow. The most recent project I finished was for my dissertation was an exploration into creating a Sign Language Conversational Agent. As of now, I don't quite know what I am going to do for my final year project but I'm exploring topics and ideas.

![University of Glasgow](../images/glasgow-uni.jpg)

Some of the courses I have completed from my time at the University of Glasgow are:
- Constraint Programming, Safety Critical Systems, Big Data, Programming Languages
- Computer Vision, Functional Programming
- Text as Data, Artificial Intelligence, Cyber Security, Database Systems
- Systems Programming, Data Science, Interactive Systems, Algorithmics
- Web App Development, Java Programming, Python Programming

I have also won the awards for highest achieving student for both 2nd and 3rd year computing science.

## Hobbies

I have a variety of hobbies that showcase my inability to specialise in any one area or topic. However, I like to think that it makes me a more well rounded person *( or at least I hope so :> )*.

#### Sports

Although I am from the UK I have been a fan of American Football for a while, supporting the Atlanta Falcons. I am also interested in road cycling. I also catch up on Formula 1 as a new recent sports interest.

Finally, I am super energetic about the [Marble League](https://www.youtube.com/channel/UCYJdpnjuSWVOLgGT9fIzL0g) - the only marble sport with real teams, competitions and marbles! I support the Midnight Wisps if you're wondering (you can usually find the picture below as my avatar) ⤵️

![Midnight Wisps Logo](../images/midnight-wisps-logo.jpg)

#### Gaming

I enjoy spending some of my free time playing games - especially co-op with my partner. I mostly enjoy story-heavy games rather than fast paced action games like FPS games.

I've also made a game or two with an interest in development of games. Furthermore, I sometimes playtest games as well.

#### Music

It is a bit weird for me to say that I have had quite a long history of music. I initially started learning to play the clarinet nearly 10 years ago. I have also had some time playing percussion within the Stewarton Wind Band. Now I try to keep playing these intruments when I can but also try to create some music digitally for games that I create with people.

#### Learning Languages

I quite like learning languages even though I am not particularly good at it. However, I was bilingual from birth but in an unexpected way. My parents are deaf and so my first language is actually British Sign Language (BSL). If you're wondering I picked up English in the same way as BSL.

I have tried to learn quite a few languages. But I am finally learning a language properly and that is Polish since my Partner is Polish (yes it is bloody difficult so I need all the luck I can get). Maybe a future language might be Japanese or perhaps Japanese Sign Language would be a more interesting choice. Either way, I should really just focus on one language for now!

![Grzegorz Brzęczyszczykiewicz](../images/polish-joke.jpg "Grzegorz Brzęczyszczykiewicz - The best polish name and character in the world")
