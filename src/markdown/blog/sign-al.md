---
title: "SignAl - The Sign Language Conversational Agent"
last_update: "2020-07-02"
description: "SignAl is a conversational sign language agent exploration done for my 4th year project dissertation. I talk about my experiences and reflect on what could be done better and what could be continued to be worked on."
---

## Background

SignAl was a project I worked on that explored the feasibility of creating a sign language conversational agent. The biggest contribution of the project was the creation of a 2D virtual agent that could sign any phrase using the BSL alphabet without any manual intervention.

`youtube: https://youtu.be/7p8U6BFdHBI`

### Level 4 Dissertation

A major part of the 4th year of a Computing Science student at Glasgow University is spent on an individual project. It is worth so much that it makes up one third of the grade for that year!

I could have chosen a preference from a list of projects published by professors at the university. However, I wanted to take the initiative and not leave my project to fate. And I think if anyone is going to work on something significant over the one third of the next year that they better enjoy it (...or you might be in for a rough time).

Thus, I chose to self-define my project so I could work on what I wanted to and also make sure that I met with potential supervisors to guage whether we would be a good match for working together. This is a great piece of advice that my friend and former flatmate Malin told me while she was starting a PhD.

### Why Sign Language?

I have a strong drive to work on things that make the world a better place. So, I guess that is one reason why I chose something like this that has a clear application and would really help people. It is also perhaps an area less looked at due to deaf people being a minority in the world. There are many companies that work on translation of spoken languages but there is hardly any useful technology applications for deaf people used in the world.

I also have a background in British Sign Language. Many people don't realise it but it is actually my first language as I have 2 deaf parents. So, in a way this project is also personal to me and my family and a way to put what I've learned to good use.

### Supervisor

My supervisor for this project and this year was Mary Ellen Foster. Mary Ellen fitted with the project relatively well due to the majority of her work being with interactive agents, either through robots or voice assistants. She also had even more relevant experience with some work done regarding interactive agents with autistic people - autistic people share a similarity with deaf people in that they sometimes interact with others in the world through a sign language rather than vocal communication. Mary Ellen was a great supervisor to work with and I am very grateful to have had her as my supervisor to help me through this project. You can find more about her at her [website](http://www.dcs.gla.ac.uk/~mefoster/)

![Mary Ellen Foster and her robot Pepper](../../images/mef.png)

## What I learned

The project was split up into two parts, gesture recognition and animation synthesis, with my time being split relatively equally between them.

### Gesture Recognition

Currently gesture recognition is still a field where the accuracy of the state-of-the-art solutions is still relatively low. I learned a lot about current deep learning techniques during my exploration of this area for my virtual agent but at the same time my efforts in this area were not successful. This was definitely a weak point of my project.

Reflecting back on this, I believe that there were a few reasons that this part of the project did not end up being successful. Firstly, it was just too ambitious to try and get both gesture recognition and animation done within the time I had. Gesture recognition is usually a sizeable project on its own with lots of time needed to train the neural networks and to collect the datasets. Another reason was that many of the techniques still require large resources such as extremely powerful GPUs to handle the large architectures within the deep learning models. Thus, I believe that a reliable, fast and lightweight sign recognition module was never truly feasible using current techniques.

### Animation

In stark contrast, the animation synthesis side of things when rather well! I created an animation synthesis pipeline that managed to mimic video examples into the animation of a 2D virtual agent without any manual intervention. The first stage of the pipeline is to take example videos of sign language and use Openpose (a pose estimator library that can work in real time) to estimate the poses within these videos. I then used that information with pyglet (a python game engine) to move a 2D agent which was a collection of body parts that when individually moved into positions can create a full person.

![Virtual Agent vs Human Agent Pose Comparison](../../images/signal-pose.png)

## Evaluation

However, I did need to test whether the animations that the synthesis pipeline created were any good. One of the main worries is that whether the 2D animations were incomprehensible since you obviously lose a bit of depth information. Thus, I set up a user comprehensibility test where I compared the comprehensibility of the virtual agent to a human agent on a small collection of phrases. The users simply needed to write down what they think the agent said in the BSL alphabet.

Predictably, the virtual agent was less comprehensible than the human agent, however, the results from the virtual agent were promising as it was clear that the virtual agent was partly comprehensible - sometimes the majority of the time.

## What now? and where can you find out more?

Of course what I have mentioned here omits a lot of important points and information so if you wish to find out more you can read my [dissertation](../../data/dissertation.pdf) (though it is a bit long...) and check out the [repo](https://gitlab.com/MatthewMcConnell/sign-al) itself!

As for whether I am going to continue work on this... **_I'm not sure!_**. I think a true sign language conversational agent project would likely need more time something like a PhD perhaps. But right now I'm not sure how much more I could do with it and I'm really still exploring topics to figure out what I love doing in computing science. However, at the time of writing I am working on an extended abstract submission to the [IVA 2020](https://iva2020.psy.gla.ac.uk/) for the animation synthesis part of my project. So, hopefully all will go well and I'll have a successful submission - wish me luck!
