---
title: Twine Game Jam Workshop
last_update: 2020-08-26
description: In July I helped deliver a student graduate attributes workshop in creating literary games! Throughout this I passed on knowledge and advice about making games from previous game jams while also learning quite a bit about creating a literary-based game.
---

## The What

Within the second semester GUDEV (Glasgow University Game Development Society) was contacted regarding getting involved in a graduate skills attributes event. We were contacted by Laura Tansley who is a Graduate Attributes and Work-Based Learning Tutor at the School of Critical Studies in the University of Glasgow. Laura was organising a literary game making intro session(or crash course if you prefer). Within this we planned to go over literary gaming, the game making process, and to make a literary game together on [twine](https://twinery.org)!

![Twine Logo](https://i.pcmag.com/imagery/reviews/04LyM77XYtIjqCfR2P34DvA-12..1600459271.png)

## What I did to prepare

For this event I created a [presentation](https://docs.google.com/presentation/d/1QlOuk5f0YOcPKMcOG5CXV56tnLxTNJoXvlADt15NQ98/edit?usp=sharing) as the base of my talk and discussion with students that were attending. An interesting aspect was that I had never had any experience of literary games so linking it and making sure I didn't repeat information from other parts of the session was important. I also had to bear in mind that students attending were probably people who had never considered making a game before, so laying out the game making process from the perspective of a newbie was important.

![single slide of my presentation](../../images/twine-presentation.png)

## What happened

We successfully ran an afternoon session to 2 students! While this is a low number I was glad that we managed to get more than one student (especially during the height of the COVID summer). And of course on top of the discussions around literary games we made a start on a game! The ideas that came out were quite funny and intriguing as we used a consequences style mini-game to mix parts of game ideas up.