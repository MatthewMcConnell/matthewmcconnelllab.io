---
title: "ProtoMolecule - A Graph Database Prototyping Tool"
last_update: "2021-05-18"
description: "During my work on my 5th-year project I created the beginning of a graph database prototyping tool. This was born out of some frustration of the lack of a tool in the database community to easily learn, create, and explore graph database designs."
---

After a gruelling final year at the University of Glasgow, I have finished my Master's project relating to graph databases. Following on from my fourth-year project, I felt as though I had still new areas of computing science to explore. I had developed an interest in data storage from taking a Big Data course in the previous year and thought that it would be a good experience to explore this area in case it was one that I wished to continue working in.

From this inspiration, I reached out to Nikos Ntarmos who lectured the Big Data course at the University of Glasgow. We went through a couple of ideas surrounding graph databases or graph-related applications. It wasn't anything that I had much experience in at all - most of the Big Data course focused on Big Data storage and processing e.g. Hadoop, Spark, Cassandra. This contributed to me struggling with this project in the beginning but also ended up showing a major gap in the database research community. We found that there were no tools to allow people to explore graph database designs.

In general, there are very few tools that provide a platform for people to truly learn about and prototype graph database designs. To provide such a platform would be a solution to these barriers to contributing to graph database research. As such our goal with our work was to better help people in the following areas:

- learning
- development
- exploration

So, we had in our mind this *all-in-one* platform or tool that would help people with graph database designs. The only thing that remained was to work towards achieving that. We had many variations on what the tool should be like, including consideration of future versions that would not be feasible to complete within the short timeframe we had of an academic year. However, in the end we settled on making a platform that could represent and run a core graph database design (data storage and graph representation (data layout)). We did this through defining interfaces in Java to specify common behaviour for layers of graph database designs (see the image below).

![ProtoMolecule System Design](../images/../../images/protomolecule-current.png)

There are obviously many more details in the idea and implementation of the core framework for ProtoMolecule. However, it would be too much to detail them all here. In the end, our tool in its current form can represent graph database designs and can run a working graph database. We believe that this tool can help people learn about graph database designs and become a platform to increase productivity in the graph database research community. If you want to find out more then you can:

- check out the [repository](https://gitlab.com/MatthewMcConnell/msci-project) on gitlab
- check out the [JavaDoc documentation](https://matthewmcconnell.gitlab.io/msci-project/)
- read the [paper](../../data/paper.pdf) I wrote about it (unpublished)