import Typography from "typography"
import irvingTheme from "typography-theme-irving"

const typography = new Typography(irvingTheme)

// Export helper functions
export const { scale, rhythm, options } = typography

options["googleFonts"] = [
  {
    name: "Exo",
    styles: ["400", "700"],
  },
  {
    name: "Yrsa",
    styles: ["400", "700"],
  },
  {
    name: "Source Code Pro",
    styles: ["400"],
  },
]

export default typography
