import React from "react"
import Content from "../components/content"
import { tiles } from "./tiles.module.css"

const Tiles = ({ children }) => (
  <Content>
    <div className={tiles}>{children}</div>
  </Content>
)

export default Tiles
