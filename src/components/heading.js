import React from "react"
import { header } from "./layout.module.css"

const Heading = props => (
  <div className={header}>
    <h1>{props.text}</h1>
    <h4>{props.subText}</h4>
  </div>
)

export default Heading
