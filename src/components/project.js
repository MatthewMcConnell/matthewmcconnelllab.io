import React from "react"
import { GatsbyImage } from "gatsby-plugin-image"
import githubIcon from "../images/github-icon.png"
import gitlabIcon from "../images/gitlab-icon.png"
import musicIcon from "../images/music-icon.png"
import gameIcon from "../images/game-icon.png"
import {
  background,
  projectText,
  projectTitle,
  overlay,
  tile,
  icon,
} from "./project.module.css"
import { Link } from "gatsby"

const Project = props => {
  console.log(props.node.fields.image)
  return (
    <div className={tile}>
      {/* Background image */}
      <GatsbyImage
        image={props.node.fields.image.childImageSharp.gatsbyImageData}
        className={background}
      />
      {/* Information and link to project */}
      <a href={props.node.url} className={projectText}>
        <h5 className={projectTitle}>{props.node.name}</h5>
        <div className={overlay}>
          {props.node.description}
          <br />
          {props.node.example_url && (
            <div>
              <a href={props.node.example_url}>Try it out!</a>
            </div>
          )}

          {props.node.blog_post_link && (
            <div>
              <Link to={`/blog/${props.node.blog_post_link}/`}>Blog Post</Link>
            </div>
          )}
          <img src={getIcon(props.node.type)} alt="hello" className={icon} />
        </div>
      </a>
    </div>
  )
}

// Returns the icon image for the corresponding project type
const getIcon = type => {
  switch (type) {
    case "github":
      return githubIcon
    case "gitlab":
      return gitlabIcon
    case "music":
      return musicIcon
    case "game":
      return gameIcon
    default:
      break
  }
}

export default Project
