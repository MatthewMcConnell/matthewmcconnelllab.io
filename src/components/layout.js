import React from "react"
import { Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"
import {
  grid,
  home,
  notHome,
  nav,
  footer,
  footerIcon,
} from "./layout.module.css"

const Layout = ({ children, isHome }) => (
  <div
    className={`${grid}
     ${isHome ? home : notHome}`}
  >
    {!isHome && (
      <div className={nav}>
        <Link to="/">
          <h5>
            Matthew
            <br />
            McConnell
          </h5>
        </Link>
        <Link to={"/about/"}>
          <StaticImage src="../images/about-nav.png" alt="About" />
        </Link>
        <Link to={"/portfolio/"}>
          <StaticImage src="../images/portfolio-nav.png" alt="Portfolio" />
        </Link>
        <Link to={"/blog/"}>
          <StaticImage src="../images/blog-nav.png" alt="Blog" />
        </Link>
      </div>
    )}

    {children}
    <footer className={footer}>
      <a href="https://github.com/MatthewMcConnell">
        <StaticImage
          src="../images/github-icon.png"
          height={32}
          className={footerIcon}
          alt="My Github Profile"
        />
      </a>
      <a href="https://gitlab.com/MatthewMcConnell">
        <StaticImage
          src="../images/gitlab-icon.png"
          height={32}
          className={footerIcon}
          alt="My Gitlab Profile"
        />
      </a>
      <a href="https://www.linkedin.com/in/mm-04/">
        <StaticImage
          src="../images/linkedin-icon.png"
          height={32}
          className={footerIcon}
          alt="My Linkedin Profile"
        />
      </a>
      <br />
      <a href="mailto:matthewmcconnell04@gmail.com">
        matthewmcconnell04@gmail.com
      </a>
    </footer>
  </div>
)

export default Layout
