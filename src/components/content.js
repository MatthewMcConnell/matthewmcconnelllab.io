import React from "react"
import { main } from "./layout.module.css"

const Content = ({ children }) => <div className={main}>{children}</div>

export default Content
