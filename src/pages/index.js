import React from "react"
import Layout from "../components/layout"
import Heading from "../components/heading"
import Content from "../components/content"
import Atom from "../components/atom"
import Seo from "../components/seo"

const HomePage = () => (
  <Layout isHome={true}>
    <Seo
      title="Computing Scientist"
      description="I'm a creator, developer, and explorer who's always open to hearing about new opportunities and projects!"
    />
    <Heading
      text="Matthew McConnell"
      subText="Creator, Developer, Explorer..."
    />
    <Content>
      <div style={{ textAlign: "center" }}>
        <Atom style={{ maxHeight: "64vh" }} />
      </div>
    </Content>
  </Layout>
)

export default HomePage
