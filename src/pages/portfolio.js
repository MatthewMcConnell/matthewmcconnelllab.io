import React from "react"
import Layout from "../components/layout"
import Project from "../components/project"
import { graphql } from "gatsby"
import Heading from "../components/heading"
import Tiles from "../components/tiles"
import Seo from "../components/seo"

const PortfolioPage = ({ data }) => (
  <Layout>
    <Seo
      title="Portfolio"
      description="Projects worked on in university, game jams, and work. Explore the code repositories, try out the things I've made, and read up on my experiences of working on these projects."
    />
    <Heading text="Portfolio" />
    <Tiles>
      {data.allProjectsJson.edges.map(({ node }) => (
        <Project node={node} />
      ))}
    </Tiles>
  </Layout>
)

// grabs all the details needed about a project to be passed through
// to the project component
export const projects = graphql`
  query projects {
    allProjectsJson(sort: { order: DESC, fields: last_worked_on }) {
      edges {
        node {
          fields {
            image {
              childImageSharp {
                gatsbyImageData
              }
            }
          }
          description
          name
          url
          type
          example_url
          blog_post_link
        }
      }
    }
  }
`

export default PortfolioPage
