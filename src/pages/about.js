import React from "react"
import Layout from "../components/layout"
import { graphql } from "gatsby"
import Heading from "../components/heading"
import Content from "../components/content"
import Seo from "../components/seo"

const AboutPage = ({ data }) => {
  return (
    <Layout>
      <Seo
        title="About"
        description="Everything you need to know about me and why you should work with me!"
      />
      <Heading text="About" />
      <Content>
        <div dangerouslySetInnerHTML={{ __html: data.markdownRemark.html }} />
        <br />
        <i>Last updated {data.markdownRemark.frontmatter.last_update}.</i>
      </Content>
    </Layout>
  )
}

export const query = graphql`
  query MyQuery {
    markdownRemark(frontmatter: { title: { eq: "About" } }) {
      html
      frontmatter {
        last_update(fromNow: true)
      }
    }
  }
`

export default AboutPage
