import React from "react"
import Layout from "../components/layout"
import { graphql, Link } from "gatsby"
import Heading from "../components/heading"
import Content from "../components/content"
import Seo from "../components/seo"

const BlogPage = ({ data }) => (
  <Layout>
    <Seo
      title="Blog"
      description="Occasional blog posts about computing science, what I'm working on, and other curiosities."
    />
    <Heading text="Blog" />
    <Content>
      <ul>
        {data.allMarkdownRemark.edges.map(({ node }) => (
          <li>
            <h3>
              <Link to={node.fields.slug}>{node.frontmatter.title}</Link>{" "}
              <h6>{node.frontmatter.last_update}</h6>
            </h3>
          </li>
        ))}
      </ul>
    </Content>
  </Layout>
)

export const blog_links = graphql`
  {
    allMarkdownRemark(
      filter: { fileAbsolutePath: { regex: "/blog/" } }
      sort: { fields: frontmatter___last_update, order: DESC }
    ) {
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            title
            last_update(formatString: "DD MMMM YYYY")
          }
        }
      }
    }
  }
`

export default BlogPage
